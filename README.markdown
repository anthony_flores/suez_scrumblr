scrumblr
========

how to install and run on your own computer (linux/osx)
-------------------------------------------------------

- [install redis](http://redis.io/download)
-       sudo apt update
-       sudo apt install redis-server

- [install node.js](http://nodejs.org/)
-       sudo apt install nodejs npm
-       sudo apt install nodejs-legacy

- cd to the scrumblr directory; you should see server.js and config.js and other files.
-     cd scrumblr
- run npm
-       npm install
- run redis 
-       redis-server
- run scrumblr where "80" is the port you have opened in your firewall and want scrumblr to run on. 
-       node server.js --port 80 
- open a browser to `http://<server>:<port>` where `<server>` is your server's url or IP address, and `<port>` is the port you chose in the previous step.

license
-------

scrumblr is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

scrumblr is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

See <http://www.gnu.org/licenses/>.

the *images* used in scrumblr, however are licensed under cc non commercial noderivs:

<a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/3.0/"></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/3.0/">Creative Commons Attribution-NonCommercial-NoDerivs 3.0 Unported License</a>.

Author
-------

correction : See <https://github.com/aliasaria/scrumblr/issues/110>.
